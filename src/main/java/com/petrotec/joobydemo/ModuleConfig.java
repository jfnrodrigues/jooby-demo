package com.petrotec.joobydemo;

import com.google.inject.Binder;
import com.petrotec.joobydemo.service.DemoService;
import com.petrotec.joobydemo.service.DemoServiceImpl;
import com.typesafe.config.Config;
import org.jooby.Env;
import org.jooby.Jooby;

public class ModuleConfig implements Jooby.Module {
	@Override
	public void configure(Env env, Config conf, Binder binder) throws Throwable {
		binder.bind(DemoService.class).to(DemoServiceImpl.class);
	}
}
