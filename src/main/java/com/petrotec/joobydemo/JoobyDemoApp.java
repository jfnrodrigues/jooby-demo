package com.petrotec.joobydemo;

import org.jooby.Jooby;
import org.jooby.apitool.ApiTool;
import org.jooby.scanner.Scanner;

public class JoobyDemoApp extends Jooby {

	{
		//DI config
		use(new ModuleConfig());

		//Fileclasspath scanner
		use(new Scanner());

		//Api Documentation - Swagger or raml
		use(new ApiTool()
				//				.swagger("/swagger")
				.raml("/raml"));

		//HIbernate
		//		use(new Jdbc());
		//		use(new Hbm()
		//				.classes(Beer.class));
	}

	public static void main(String... args) {
		run(JoobyDemoApp::new, args);
	}
}
