package com.petrotec.joobydemo.controller;

import com.petrotec.joobydemo.service.DemoService;
import org.jooby.mvc.Consumes;
import org.jooby.mvc.GET;
import org.jooby.mvc.Path;
import org.jooby.mvc.Produces;

import javax.inject.Inject;
import java.time.LocalDateTime;
import java.util.List;

@Path("/demo")
@Consumes("application/x-www-form-urlencoded")
@Produces("application/json")
public class DemoController {

	private final DemoService demoService;

	@Inject
	public DemoController(DemoService demoService) {
		this.demoService = demoService;
	}

	@Path("/helloWorld")
	@GET
	public String helloWorld() {
		return demoService.helloWorld();
	}

	@Path("/helloWorldList")
	@GET
	public List<String> helloWorldList() {
		return demoService.helloWorldList();
	}

	@Path("/timestamp")
	@GET
	public LocalDateTime timestamp() {
		return demoService.timestamp();
	}

}
